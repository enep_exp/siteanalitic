from PySide2.QtWidgets import QMainWindow
from PySide2.QtWidgets import QVBoxLayout,QLineEdit,QPushButton,QWidget

class SiteAudit(QMainWindow):
    """Main Windows"""
    
    title:str
    edit:QLineEdit
    btn:QPushButton

    def __init__(self,parent = None):
        super(SiteAudit,self).__init__(parent)
        self.title = "ENEP Lab. SiteAudit"
        panel = QWidget()
        vbox = QVBoxLayout()
        self.edit = QLineEdit("Write text")
        self.btn = QPushButton("Press button")

        vbox.addWidget(self.edit)
        vbox.addWidget(self.btn)

        panel.setLayout(vbox)
        self.setCentralWidget(panel)
        self.setWindowTitle(self.title)




