"""
Module for working HTML page for SEO
"""
from bs4 import BeautifulSoup

DOCTAGS: list[str] = ["h1", "h2", "h3", "h4", "h5", "h6", "p", "b", "u", "i", "pre"]
MEDIATAGS: list[str] = ["img", "a", "video", "audio", "a"]

TAGS: list[str] = [*DOCTAGS, *MEDIATAGS]


class MetaTags(object):
    """ Tag META"""
    Description: str = ""
    Keywords: str = ""

    def __init__(self, description: str, keywords: str):
        """MetaTags class constructor and basic mata tag parser.
        :param descritption: str - Description Meta tag by HTML Page
        :param keywprds: str - Keywords meta teg by HTNL Page
        :return : MetaTags
        """
        self.Description = description
        self.Keywords = keywords


class DocContentTag(object):
    """ Tag class, HTML format, displayed text content"""
    Name: str = ""
    Content: list[str]

    def __init__(self, name: str, content: list):
        """Constructor class DocContentTag
<name>content</name>

        :param name: str name HTML tag
        :param content: list -  Inside content HTML tag
        :return: DocContentTag
        """
        self.Name = name
        self.Content = content


class MediaContentTag(object):
    """ Tag class, HTML format, media (images, video, etc) content"""
    Name: str = ""
    Src: str = ""
    Title: str = ""
    AltTitle: str = ""

    def __init__(self, name: str = "", title: str = "", alt: str = "", source: str = ""):
        """ Constructor class MediaContentTag
             <name src='source' title='title' alt='alt' />
        :param name: str - Name tag
        :param title: str - Title media file
        :param alt: str - Alternate media file
        :param source: str - URL media file
        :return MediaContentTag
        """
        self.Name = name
        self.Title = title
        self.AltTitle = alt
        self.Src = source


class HTMLPage(object):
    """ HTML Page """
    Soup: BeautifulSoup = None
    Title: str = ""
    Meta: MetaTags = None
    ListDocTags: list[DocContentTag] = None
    ListMediaTags: list[MediaContentTag] = None

    def __init__(self, content: str = " ", soup: BeautifulSoup = None):
        """HTMLPage class constructor and main tag parser for SEO.
        :param content - argument type string
        :param soup - class BeautifySoup
        :return: HTMLPage
        """

        if soup is None:
            self.Soup = BeautifulSoup(content, "lxml")
        else:
            self.Soup = soup
        self.Title = self.Soup.head.title.string
        m_v = self.Soup.find("meta", name="Description")["content"]
        m_k = self.Soup.find("meta", name="Keywords")["content"]
        self.Meta = MetaTags(m_v, m_k)
        body = soup.find("body")
        for tagname in TAGS:
            seltags = body.find_all(tagname)
            dct: list[str] = []
            for st in seltags:
                dct.append(st.text)
            self.ListDocTags.append(DocContentTag(tagname, dct))

