from enum import Enum
from bs4 import BeautifulSoup

from htmltags import HTMLPage


class TypeSource(Enum):
    TYPE_WEB = 1
    TYPE_FILE = 2
    TYPE_STRING = 3


class WebPage(object):
    Page: HTMLPage
    Count: int = 0

    def __init__(self, page: list[HTMLPage] = None):
        """
          Conscructor WebPage class
        :param page: HTMLPage - An instance of the HTMLPage class
        :return WebPage
        """
        if page is not None:
            self.Page = page[:]
            self.Cout = len(page)


class SEOWeb(object):
    """
     Class WEB site for SEO Analize
    """
    pass


