from PySide2.QtWidgets import QApplication
import sys
import ui

if __name__ == "__main__":
    app = QApplication(sys.argv)

    sa = ui.SiteAudit()
    sa.show()
    sys.exit(app.exec_())

